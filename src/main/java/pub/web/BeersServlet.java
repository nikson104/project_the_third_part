package pub.web;

import pub.web.command.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "BeersServlet",
        description = "Beers Servlet",
        urlPatterns = "/beers/*")
public class BeersServlet extends HttpServlet {
    private Map<CommandKey, Command> commands = new HashMap<>();

    {
        commands.put(new CommandKey("GET", "/beers"), new ListCommand());
        commands.put(new CommandKey("GET", "/beers/add"), new GetAddCommand());
        commands.put(new CommandKey("POST", "/beers/delete"), new DeleteCommand(
                commands.get(new CommandKey("GET", "/beers"))));
        commands.put(new CommandKey("POST", "/beers"), new AddCommand(
                commands.get(new CommandKey("GET", "/beers"))
        ));
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Command command = commands.get(new CommandKey(req.getMethod(), req.getRequestURI()));
        command.execute(req);
        RequestDispatcher dispatcher = getServletContext()
                .getRequestDispatcher("/WEB-INF/main.jsp");
        dispatcher.forward(req, resp);
    }
}