package pub.web.command;

import pub.entity.Beer;
import pub.entity.Beers;
import pub.impl.ImplBeerDao;

import javax.servlet.http.HttpServletRequest;

public class DeleteCommand implements Command {
    private Command listCommand;

    public DeleteCommand(Command listCommand) {
        this.listCommand = listCommand;
    }

    @Override
    public void execute(HttpServletRequest request) {
        int beerId = Integer.parseInt(request.getParameter("beerId"));
        Beer beer = Beers.INSTANCE.findById(beerId).get();
        Beers.INSTANCE.delete(beer);

        listCommand.execute(request);
    }
}