package pub.web.command;

import pub.entity.Beers;

import javax.servlet.http.HttpServletRequest;

public class ListCommand implements Command {
    @Override
    public void execute(HttpServletRequest request) {
        request.setAttribute("beers", Beers.INSTANCE.getBeers());
        request.setAttribute("page", "/WEB-INF/list.jsp");
    }
}