package pub.web.command;

import pub.entity.Beer;
import pub.entity.Beers;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

public class AddCommand implements Command {
    private Command listCommand;
    private int counter = 0;

    public AddCommand(Command listCommand) {
        this.listCommand = listCommand;
    }

    @Override
    public void execute(HttpServletRequest request) {
        Beers.INSTANCE.add(
                new Beer.Builder()
                        .id(counter++)
                        .price(Integer.valueOf(request.getParameter("price")))
                        .name(request.getParameter("name"))
                        .country(request.getParameter("country"))
                        .visible(Boolean.valueOf(request.getParameter("visible")))
                        .producedTime(LocalDate.parse(request.getParameter("producedTime")))
                        .build()
        );

        listCommand.execute(request);
    }
}