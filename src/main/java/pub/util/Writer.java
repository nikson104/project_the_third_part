package pub.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import pub.entity.Beer;

public class Writer implements Runnable {
    private final String fileName;
    private final String result;

    public Writer(String fileName, String result) {
        this.fileName = fileName;
        this.result = result;
    }

    @Override
    public void run() {
        System.out.println("Result: " + result + "Thread: " + Thread.currentThread().getName());
        try {
            FileWriter writer = new FileWriter(fileName + ".txt", false);
            writer.write(result);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}