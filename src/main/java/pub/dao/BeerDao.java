package pub.dao;

import pub.entity.Beer;

import java.util.List;

public interface BeerDao extends IDao<Beer>{
    Beer get(long id) throws Exception;
    Beer create(Beer t) throws Exception;
    void update(Beer t)throws Exception;
    void delete(long id)throws Exception;
    List<Beer> getAll() throws Exception;
}