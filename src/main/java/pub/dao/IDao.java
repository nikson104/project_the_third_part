package pub.dao;

import java.util.List;

public interface IDao <T> {
    T get(long id) throws Exception;
    T create(T t)throws Exception;
    void update(T t)throws Exception;
    void delete(long id) throws Exception;
    List<T> getAll() throws Exception;
}

