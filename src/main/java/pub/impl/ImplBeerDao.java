package pub.impl;

import pub.dao.BeerDao;
import pub.dao.DataSource;
import pub.entity.Beer;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ImplBeerDao implements BeerDao {
    private DataSource ds = DataSource.getInstance();
    private static final ImplBeerDao INSTANCE = new ImplBeerDao();
    private Connection connection = null;
    private int counter = 0;

    public static ImplBeerDao getInstance() {
        return INSTANCE;
    }

    @Override
    public Beer get(long key) throws Exception {
        this.connection = ds.getConnection();
        Beer beer = null;
        String sql = "SELECT b.id,b.name,b.country,b.producedTime,b.price,b.visible"
                + " FROM BEER b  WHERE b.id=?;";
        PreparedStatement stm = connection.prepareStatement(sql);
        stm.setLong(1, key);
        ResultSet rs = stm.executeQuery();
        while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            String country = rs.getString(3);
            Date producedTime = rs.getDate(4);
            int price = rs.getInt(5);
            boolean visible = rs.getBoolean(6);

            beer = new Beer(id, name, country, producedTime.toLocalDate(), price, visible);
        }
        connection.close();
        return beer;
    }

    @Override
    public Beer create(Beer beer) throws Exception {
        int idBeer = counter++;
        String name = beer.getName();
        int price = beer.getPrice();
        String country = beer.getCountry();
        LocalDate producedTime = beer.getProducedTime();
        boolean visible = beer.isVisible();
        String sql = "insert into BEER (id, name, country, producedTime, price, visible) values (" + idBeer + "," + name + "," + country + "," + producedTime + "," + price + "," + visible + ");";
        PreparedStatement stm = connection.prepareStatement(sql);
        int rs = stm.executeUpdate();
        beer.setId(idBeer);
        return beer;
    }

    @Override
    public void update(Beer t) throws Exception {

    }

    @Override
    public void delete(long id) throws Exception {
        String sql = "DELETE FROM BEER WHERE id=" + id + ";";
        PreparedStatement stm = connection.prepareStatement(sql);
        int rs = stm.executeUpdate();
    }

    @Override
    public List<Beer> getAll() throws Exception {
        this.connection = ds.getConnection();
        Beer beer = null;
        String sql = "SELECT b.id,b.name,b.country,b.producedTime,b.price,b.visible"
                + " FROM BEER b ";
        PreparedStatement stm = connection.prepareStatement(sql);
        ResultSet rs = stm.executeQuery();
        List<Beer> list = new ArrayList<>();
        while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            String country = rs.getString(3);
            Date producedTime = rs.getDate(4);
            int price = rs.getInt(5);
            boolean visible = rs.getBoolean(6);

            beer = new Beer(id, name, country, producedTime.toLocalDate(), price, visible);
            list.add(beer);
        }
        connection.close();
        return list;
    }
}