create table BEER
(
  id           int not null primary key,
  name         varchar(255),
  country      varchar(255),
  producedTime date,
  price        int,
  visible      BOOLEAN
);
