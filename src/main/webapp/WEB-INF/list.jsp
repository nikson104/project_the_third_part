<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h2>List</h2>
<table style="display: contents">
    <thead>
    <tr>
        <td style="border: solid 1px black">Name</td>
        <td style="border: solid 1px black">Country</td>
        <td style="border: solid 1px black">Price</td>
        <td style="border: solid 1px black">ProducedTime</td>
    </tr>
    </thead>
    <tbody style="border: solid 1px black">
    <c:forEach var="beer" items="${beers}">
        <c:if test="${beer.visible}">
            <tr>
                <td style="border: solid 1px black">
                        ${beer.name}
                </td>
                <td style="border: solid 1px black">
                        ${beer.country}
                </td>
                <td style="border: solid 1px black">
                        ${beer.price}
                </td>
                <td style="border: solid 1px black">
                        ${beer.producedTime}
                </td>
                <td style="border: solid 1px black">
                    <form action="/beers/delete" method="post">
                        <input type="submit" name="delete_beer" value="Delete" />
                        <input type="hidden" name="beerId" value="${beer.id}" />
                    </form>
                </td>
            </tr>
        </c:if>
    </c:forEach>
    </tbody>
</table>