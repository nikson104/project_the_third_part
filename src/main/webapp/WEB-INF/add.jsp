<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h2>Add</h2>
<form name="BeersForm" action="/beers" method="POST">
    <table>
        <tr>
            <td>Name :</td>
            <td><input type="text" name="name"/></td>
        </tr>
        <tr>
            <td>Country :</td>
            <td><input type="text" name="country"/></td>
        </tr>
        <tr>
            <td>Price :</td>
            <td><input type="text" name="price"/></td>
        </tr>
        <tr>
            <td>Produced time :</td>
            <td><input type="date" name="producedTime"/></td>
        </tr>
        <tr>
            <td>Visible :</td>
            <td><input type="checkbox" name="visible" value="true"/></td>
        </tr>
        <th><input type="submit" value="Submit" name="save"/></th>
    </table>
</form>